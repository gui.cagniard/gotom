# Gotom

## Name
This API is inspired by the Sutom game (https://sutom.nocle.fr/). This is a play on words between the language that is used to develop this API Go and the Sutom game

## Description
It is a micro-service architecture which takes the bases of the sutom game. The first service communicates with a Postgres SQL database, it allows to retrieve a random French word and also to check if a word exists in our data dictionary. The second service is a wrapper of the first one, it allows to retrieve a word by calling the first service and hiding the letters except the first one. We can also do a post query to check if the word you propose matches with the original word.

## Golang
We choose to develop the project with the language GO:

| Advantages | Disadvantages |
|-----------|-----------|
| Easy to learn | Error handling |
| Fast and easy | Security problem at runtime |
| Multithreading |       
| Adapt for microservice architecture |

## Requirements 
You must have docker, docker-compose and also golang version 1.18 installed

## Installation
To launch the project locally :  
``` git clone https://gitlab.com/gui.cagniard/gotom.git ```  
Move in the gotom folder :  
``` cd gotom ```  
Change to dev branch :  
``` git checkout dev ```  
And then launch containers with docker compose :  
``` docker-compose up ```

## Usage
After the launch, you can do :  
``` curl http://0.0.0.0:8081/game ```  
Expected output :
``` 
{
    "status": 200,
    "data": {
        "guess_word": "T*******",
        "original_word": "TCHITOLA"
    }
} 
```  

Other command : 
``` 
curl http://0.0.0.0:8081/game \
  --include \
  --header "Content-Type: application/json" \
  --request "POST" \
  --data '{"original_word": "BONJOUR","compare_word": "BONSOIR"}' 
```
Expected output :
```
{
    "status": 200,
    "data": {
        "word": true,
        "original_word": "BON*O*R"
    }
} 
```

## Architecture/Conception

With this diagram you can see all the possible interactions between the different services:

![interactions](docs/interactions.png)

Then, here is the architecture diagram for the dev branch, the docker compose manages all the images:

![architectureDev](docs/architectureDev.png)

Then, here is the architecture diagram for the prod branch, everything is automated via a Jenkins pipeline:

![architectureDev](docs/architectureProd.png)



## Contributing
To develop on the Gotom project, you must make a merge request or a commit on the dev branch and then make a cherry pick of the commit on the master branch which is the production branch
