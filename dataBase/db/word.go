package db

import (
	"dataBase/models"
	"math/rand"
	"time"
)

func GetRandomWord() (string, error) {
	// get random number between 0 and 149861
	rand.Seed(time.Now().Unix())
	randomInt := 1 + rand.Intn(149861-1)

	db, err := GetDB()

	var randomWord = models.Word{ID: randomInt}
	db.Table("words").Where("id = ?", randomWord.ID).Find(&randomWord)

	return randomWord.Word, err
}

func Is_exist(param string) (bool, error) {
	var word_test = models.Word{Word: param}

	db, err := GetDB()

	result := db.Table("words").Where("word = ?", word_test.Word).Find(&word_test)

	if result.RowsAffected != 1 {
		return false, err
	} else {
		return true, err
	}
}
