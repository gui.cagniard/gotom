package models

type Word struct {
	ID   int    `json:"id" gorm:"primary_key"`
	Word string `json:"word"`
}
