package controller

import (
	"dataBase/db"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetRandomWord(c *gin.Context) {
	var word, err = db.GetRandomWord()
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Error failed to connect database"})
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": word})
}

func GetExist(c *gin.Context) {
	word := c.Param("word")
	var check, err = db.Is_exist(word)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Error failed to connect database"})
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": check})
}
