package main

import (
	"dataBase/controller"
	"dataBase/db"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {

	db.Init()

	router := gin.Default()

	router.GET("/word", controller.GetRandomWord)
	router.GET("/check/:word", controller.GetExist)

	router.Run("0.0.0.0:8080")
}
