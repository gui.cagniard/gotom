package main

import (
	"net/http"
	"web/controller"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	router.GET("/game", controller.GetWord)
	router.POST("/game", controller.PostProposal)

	router.Run("0.0.0.0:8081")
}
