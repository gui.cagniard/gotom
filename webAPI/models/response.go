package models

type ResponseWord struct {
	Status int    `json: "status"`
	Data   string `json: "data"`
}

type ResponseCheck struct {
	Status int  `json: "status"`
	Data   bool `json: "data"`
}

type PostWord struct {
	Original_word string `json: "original_word"`
	Compare_word  string `json: "compare_word"`
}
