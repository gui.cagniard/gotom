package provider

import (
	"testing"
)

func TestCheckSize(t *testing.T) {
	res := CheckSize("BONJOUR", "BONSOIR")
	if res != true {
		t.Errorf("CheckSize('BONJOUR', 'BONSOIR') FAILED, Expected %t, got %t\n", true, res)
	}
}

func TestStartWithTheRightLetter(t *testing.T) {
	res := StartWithTheRightLetter("BONJOUR", "BONSOIR")
	if res != true {
		t.Errorf("StartWithTheRightLetter('BONJOUR', 'BONSOIR') FAILED, Expected %t, got %t\n", true, res)
	}
}

func TestGuessLetters(t *testing.T) {
	res := GuessLetters("BONJOUR", "BONSOIR")
	if res != "BON*O*R" {
		t.Errorf("GuessLetters('BONJOUR', 'BONSOIR') FAILED, Expected %s, got %s\n", "BON*O*R", res)
	}
}
