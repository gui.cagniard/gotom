package provider

import (
	"encoding/json"
	"net/http"
	"strings"
	"web/models"
)

func GetWordFromDataBase() (string, string, bool) {
	var response models.ResponseWord
	var db bool
	resp, err := http.Get("http://192.100.150.80:8080/word")

	if err != nil {
		db = false
		return "error", "error", db
	}

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		json.NewDecoder(resp.Body).Decode(&response)
		res := []rune(response.Data)
		var word string
		for i := 0; i < len(res); i++ {
			if i == 0 {
				word += string(res[i])
			} else {
				word += strings.Replace(string(res[i]), string(res[i]), "*", 1)
			}
		}
		db = true
		return response.Data, word, db
	} else {
		db = false
		return "error", "error", db
	}
}

func CheckWord(word string) (bool, bool) {
	var response models.ResponseCheck
	var db bool
	resp, err := http.Get("http://192.100.150.80:8080/check/" + word)

	if err != nil {
		db = false
		return false, db
	}

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		json.NewDecoder(resp.Body).Decode(&response)
		db = true
		return response.Data, db
	} else {
		db = false
		return false, db
	}
}

func CheckSize(word_original, word_compare string) bool {
	if len(word_compare) == len(word_original) {
		return true
	} else {
		return false
	}
}

func StartWithTheRightLetter(word_original, word_compare string) bool {
	rune_word_original := []rune(word_original)
	rune_word_compare := []rune(word_compare)
	if rune_word_compare[0] == rune_word_original[0] {
		return true
	} else {
		return false
	}
}

func GuessLetters(word_original, word_compare string) string {
	rune_word_original := []rune(word_original)
	rune_word_compare := []rune(word_compare)
	var res string

	for i := 0; i < len(rune_word_compare); i++ {
		if rune_word_compare[i] == rune_word_original[i] {
			res += string(rune_word_compare[i])
		} else {
			res += ("*")
		}
	}

	return res
}
