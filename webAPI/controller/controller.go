package controller

import (
	"net/http"
	"web/models"
	"web/provider"

	"github.com/gin-gonic/gin"
)

func GetWord(c *gin.Context) {
	var word_original, guess_original, db = provider.GetWordFromDataBase()
	if db == false {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Error failed to connect to the other service"})
		panic(db)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gin.H{"original_word": word_original, "guess_word": guess_original}})
}

func PostProposal(c *gin.Context) {
	var postWord models.PostWord
	if err := c.BindJSON(&postWord); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Can't retrieve the data post"})
		panic(err)
	}
	var checkSize = provider.CheckSize(postWord.Original_word, postWord.Compare_word)
	if checkSize == true {
		var startWithTheRightLetter = provider.StartWithTheRightLetter(postWord.Original_word, postWord.Compare_word)
		if startWithTheRightLetter == true {
			var check, db = provider.CheckWord(postWord.Compare_word)
			if db == false {
				c.IndentedJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Error failed to connect to the other service"})
				panic(db)
			}
			if check == true {
				var word = provider.GuessLetters(postWord.Original_word, postWord.Compare_word)
				c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gin.H{"word": true, "response:": word}})
			} else {
				c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gin.H{"word": false, "response": "The word doesn't exist"}})
			}
		} else {
			c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gin.H{"word": false, "response": "The word doesn't start with the right letter"}})
		}
	} else {
		c.IndentedJSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gin.H{"word": false, "response": "Not the same size for both words"}})
	}
}

/*curl http://0.0.0.0:8081/game \
  --include \
  --header "Content-Type: application/json" \
  --request "POST" \
  --data '{"original_word": "BONJOUR","compare_word": "BONSOIR"}'*/
